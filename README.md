This is an example to offer a chance to perform a git merge, as part of Assignment 4 for the "Introduction to Software Engineering for Computational Science" course :
https://bitbucket.org/psanan/sefcs2015 .

It is based on a C++ code written by Ben Cumming and Gilles Fourestey for use at the CSCS Summer School : https://github.com/fomics/SummerSchool2014

Your task is to merge the `features` branch into the `master` branch, to obtain a more full-featured implementation, but retain the same output. 
(The feature branch is the original code, which is better in most ways, except that due to some constraint of the project, perhaps a colleague who is relying on the exact format of the output, we must use the output format of the `master` branch)