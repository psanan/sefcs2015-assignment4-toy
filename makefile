CXX=g++
CXXFLAGS=-fopenmp

OBJ = stats.o operators.o linalg.o data.o

all: main

%.o : %.cpp 
	$(CXX) $(CXXFLAGS) -c $< -o $@

main: $(OBJ) main.cpp 
	$(CXX) $(CXXFLAGS) $^ -o $@

clean:
	rm -f main
	rm -f $(OBJ)
